
// para importar el io.js donde hemos metido la function de escribir fichero
const io = require('../io');
const jwt = require('jsonwebtoken');

//const jwt = require('../jsonwebtoken');
//var bodyParser = require('body-parser');
//app.use(bodyParser.urlencoded({extended: false}));
//app.use(bodyParser.json({limit:'10mb'}));


//******************************************
// API REALIZAR EL LOGIN Y LOGOUT
//******************************************

function loginUsersV1 (req, res) {

  console.log("POST /apitechu/v1/login");
  console.log(req.body.email);
  console.log(req.body.password);

  var users = require('../usuarios.json');
  var result = {};
  var encontrado = false;

  for (var i = 0; i < users.length; i++) {
    console.log("bucle:"+ i + "users[i]:" + users[i].email + "///" + users[i].password );
    if ((users[i].email == req.body.email) && (users[i].password == req.body.password)) {
       console.log("usuario password encontrado");
       console.log("email:" + users[i].email);
       var tokenData = {
           username: users[i].email
       }
       var jwtToken = jwt.sign(tokenData, 'Secret token', {
            expiresIn: 60 * 60 * 24 // expires in 24 hours
         });
       console.log("token jvw:" + jwtToken);
       users[i].logged = true;
       users[i].token = jwtToken;
       encontrado=true;
       io.writeUserDataToFile(users);
       break;
     }
   }

   if (encontrado) {
     result.mensaje = "Login correcto";
     result.idUsuario = users[i].id;
     result.token = users[i].token;
   } else {
     result.mensaje = "Login incorrecto";
   }
   res.send(result);
}

function logoutUsersV1 (req, res) {

  console.log("POST /apitechu/v1/logout");
  console.log(req.params.id);

  var users = require('../usuarios.json');
  var result = {};
  var encontrado = false;

  for (var i = 0; i < users.length; i++) {
    console.log(users[i].id + "///" + req.params.id );

    if ((users[i].id == req.params.id) && (users[i].logged===true)) {
       console.log("usuario encontrado y logado");
       console.log("id:" + users[i].id);
       delete users[i].logged;
       delete users[i].token;
       encontrado=true;
       io.writeUserDataToFile(users);
       break;
     }
   }


   if (encontrado) {
     result.mensaje = "logout correcto";
     result.idUsuario = users[i].id;
   } else {
     result.mensaje = "logout incorrecto";
   }
   res.send(result);
}

module.exports.loginUsersV1 = loginUsersV1;
module.exports.logoutUsersV1 = logoutUsersV1;
