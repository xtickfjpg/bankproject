
// para importar el io.js donde hemos metido la function de escribir fichero
const io = require('../io');

//******************************************
// API CONSULTA USUARIOS TOTAL Y CON FILTROS
//******************************************

function getUsersV1(req, res) {
  console.log("GET /apitechu/v1/users");

  var result = {};
  // hay que cambiar la ruta del fichero..... hay que poner .. en vez de .
  var users = require('../usuarios.json');

  if (req.query.$count == "true") {
    console.log("Count needed");
    result.count = users.length;
  }

  result.users = req.query.$top ?
    users.slice(0, req.query.$top) : users;

  res.send(200,result);
}

//********************************
// API ALTA NUEVO USUARIO
//********************************

function createtUsersV1 (req, res) {
  console.log("POST /apitechu/v1/users");
  console.log(req.body.first_name);

  var users = require('../usuarios.json');

  var newUser = {};
  console.log (req.body.id)
  if (req.body.id != null && req.body.id != undefined) {
    newUser.id = req.body.id;
  }
  newUser.first_name = req.body.first_name;
  newUser.last_name = req.body.last_name;
  newUser.email = req.body.email;
  newUser.password = req.body.password;


  users.push(newUser); //añadimos un usuario al final del array
  io.writeUserDataToFile(users);
  console.log("usuario añadido");
  res.send(201,"alta Correcta");
}

//************************
// API BORRADO USUARIO
//************************
function deleteUserV1(req, res) {
  console.log("DELETE /apitechu/v1/users/:id");
  console.log("el id a borrar es " + req.params.id);
  var users = require('../usuarios.json');

  for (var i = 0; i < users.length; i++) {
    console.log("bucle:"+ i);
    console.log("users[i]:"+ users[i].id);
     if (users[i].id == req.params.id ) {
       console.log("encuentro el elemento");
       console.log("usuario borrado." + "clave:"+ req.params.id + "elemento:" + i );
       console.log("first_name:" + users[i].first_name);
       console.log("last_name:" + users[i].last_name);
       console.log("email:" + users[i].email);
       users.splice(i,1);
       io.writeUserDataToFile(users);
       res.send(200,"Borrado realizado");
       break;
     }
   }
 }



module.exports.getUsersV1 = getUsersV1;
module.exports.createtUsersV1 = createtUsersV1;
module.exports.deleteUserV1 = deleteUserV1;
