// constantes que son el core de la aplicacion
// nos traemos el framework... y lo ponemos en marcha

const express = require('express');
const app= express();
app.use(express.json());  // dentro del express.. para pasear el body y que utilice json
const io = require('./io'); // importar io.js donde esta function de escribir fichero
const userController = require('./controller/UserController'); // importar el userController operativa de usuario
const authController = require('./controller/AuthController');

// valor por defecto a una variable de entorno
// si existe la variable de entorno port.. cogemos ese valor
// sino cogemos el port 3000.
const port = process.env.PORT || 3000;
// pone en marcha eschucar el puerto
app.listen(port);
console.log("API eschucando en el puerto:" + port );

//******************************************
// API CONSULTA USUARIOS TOTAL Y CON FILTROS
//******************************************
app.get('/apitechu/v1/users',userController.getUsersV1);
//********************************
// API ALTA NUEVO USUARIO
//********************************
app.post('/apitechu/v1/users',userController.createtUsersV1);
//************************
// API BORRADO USUARIO
//************************
app.delete('/apitechu/v1/users/:id',userController.deleteUserV1);
//************************
// API LOGIN
//************************
app.post('/apitechu/v1/login',authController.loginUsersV1);
//************************
// API logout
//************************
app.post('/apitechu/v1/logout/:id',authController.logoutUsersV1);

//********************************
// API PRUEBA DEVOLVER MSG
//********************************
app.get('/apitechu/v1/hello',
  function  (req,res) {
    console.log("GET /apitechu/v1/hello");
    //devolvemos un string que que podemos transaformar en json
    //res.send('{"msg" : "hola desde apitechu"}');
    //pero para devolver directamente en json tenemos que hacer
    // a partir de ahora.. todas las salidas de nuestros servicios tiene que
    // servicios asi.
    res.send({"msg" : "hola desde apitechu"});
  }
)

//********************************
// API PRUEBA METODOS PARA PASAR LOS
// PARAMETROS AL SERVICIOS
//********************************
app.post(
  '/apitechu/v1/monstruo/:p1/:p2',
    function  (req,res) {
      console.log("GET /apitechu/v1/monstruo");
      console.log("parametros:");
      console.log(req.params);

      console.log("Query String:");
      console.log(req.query);

      console.log("Headers:");
      console.log(req.headers);

      console.log("Body:");
      console.log(req.body);
    }
)
