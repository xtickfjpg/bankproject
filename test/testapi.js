const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();

// nuestro test hara una peticion a  www.duckduckgo.com'
/*
describe('First test',
  function() {
    it('Test that DuckDuckgo works', function(done) {
        chai.request('http://www.duckduckgo.com')
        .get('/')
        .end(
          function(err, res) {
//          console.log(res);
            console.log("Request has finisihed");
            console.log(err);
            res.should.have.status(200);
            done();
          }
        )
    }
    )
  }
)
*/

describe('Test de API usuarios',
  function() {
    it('Prueba que la API usuarios responde correctamente', function(done) {
        chai.request('http://localhost:3000')
        .get('/apitechu/v1/hello')
        .end(
          function(err, res) {
//          console.log(res);
            console.log("Request has finisihed");
            console.log(err);
            res.should.have.status(200);
            res.body.msg.should.be.eql("hola desde apitechu");
            done();
          }
        )
      }
    ),
    it('Prueba que la API devuelve una lista de usuarios correctos', function(done) {
        chai.request('http://localhost:3000')
        .get('/apitechu/v1/users')
        .end(
          function(err, res) {
            console.log("Request has finisihed");
            res.should.have.status(200);
            res.body.users.should.be.a('array'); // comprueba que sea un arrary
            for  (user of res.body.users) {
              user.should.have.property('email');
              user.should.have.property('password');
              user.should.have.property('first_name');
            }
            done();
          }
        )
      }
    ),

    it('Prueba que la API realiza alta de un nuevo usuario correctamente', function(done) {
      var newUser = {};
      newUser.id = 99;
      newUser.first_name = 'firstname_test';
      newUser.last_name = 'lastname_test';
      newUser.email = 'test@email.com';
      newUser.password = 'xisyslkjdkljdksj';
      chai.request('http://localhost:3000')
        .post('/apitechu/v1/users')
        .send(newUser)
        .end(
          function(err, res) {
            console.log("Request has finisihed");
            console.log(res.status);
            res.should.have.status(201);
            done();
          }
        )
      }
    ),

    it('Prueba que la API realiza borrado de un usuario correctamente', function(done) {
        chai.request('http://localhost:3000')
        .delete('/apitechu/v1/users/99')
        .end(
          function(err, res) {
            console.log("Request has finisihed");
            res.should.have.status(200);
            done();
          }
        )
      }
    )
  }
)
